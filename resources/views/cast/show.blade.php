@extends('layouts.master')
@section('judul')
Halaman Data Show
@endsection
@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Detail Item <?php echo $item->id ?></h1>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="<?php echo route('cast.read') ?>"></a>Item</li>
                    <li class="breadcrumb-item active">Detail Item</li>
                </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-body">
                <label>Nama :</label>
                <p <?php echo $item->nama?>></p>
                
                <label>Umur</label>
                <p <?php echo $item->umur?>></p>
                <label>Bio</label>
                <p <?php echo $item->bio?>></p>
            </div>
        </div>
    </section>
    @endsection
