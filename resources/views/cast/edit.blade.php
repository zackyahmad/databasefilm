@extends('layouts.master')

@section('judul')
Halaman Edit Cast
@endsection

@section('content')

   
 <h2>Tambah Data</h2>
        <form action="<?php echo route('cast.store') ?>" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">nama</label>
                <input type="text" class="form-control" name="nama" id="title" value="<?php echo old('nama')?>" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">umur</label>
                <input type="number" class="form-control" name="umur" id="title" value="<?php echo old('umur')?>" placeholder="Masukkan umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">biodata</label>
                <input type="text" class="form-control" name="bio" id="body" value="<?php echo old('bio')?>" placeholder="Masukkan Biodata">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit Cast</button>
        </form>

@endsection
    