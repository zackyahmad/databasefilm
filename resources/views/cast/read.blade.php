@extends('layouts.master')
@section('judul')
Halaman Data Cast
@endsection
@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Cast</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
        <div class="card">
            <div class="card-body">
                <a href="<?php echo route('cast.create') ?>" class="btn btn-primary mb-3">Tambah</a>
                <table class="table">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" class="text-center">#</th>
                        <th scope="col" class="text-center">Nama</th>
                        <th scope="col" class="text-center">Umur</th>
                        <th scope="col" class="text-center">Biodata</th>
                        <th scope="col" class="text-center">Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($items as $key => $item)
                            <tr>
                                <td><?php echo $key + 1?></th>
                                <td><?php echo $item->nama?></td>
                                <td><?php echo $item->umur?></td>
                                <td><?php echo $item->bio ?></td>
                                <td>
                                <a href="<? route('cast.show') ?>" class="btn btn-info">Show</a>
                                <a href="<? route('cast.edit') ?>" class="btn btn-info mt-2">Edit</a>
                                </td>
                            </tr>
                        @empty
                            <tr >
                                <td colspan="4" class="text-center">No data</td>
                            </tr>  
                        @endforelse              
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    @endsection
