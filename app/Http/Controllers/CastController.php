<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function read()
    {
        $items = Cast::all();
        return view('cast.read', compact('items'));
    }
    public function create(){
        return view('cast.create');
    }
    public function store(Request $request){
        $this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
    		'bio' => 'required'
    	]);
 
        Cast::create([
    		'nama' => $request->nama,
    		'umur' => $request->umur,
    		'bio' => $request->bio
    	]);
 
    	return redirect('/read');
    }
    public function show($id){
        $item = Cast::findOrFail($id);
        return view('cast.show', compact('item'));
    }
    public function edit(){
        $item = Cast::findOrFail($id);
        return view('cast.show', compact('item'));
    }
}
